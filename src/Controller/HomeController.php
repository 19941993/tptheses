<?php

namespace App\Controller;
//use App\Entity\EcoleDoctorale;
//use App\Entity\Theses;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController//héritage
{
    /**
     * @Route("/home", name="home")//quand je tape /home j'accedde à la page home
     */
    public function index()//cette fonction va traiter la demande) et elle va nous renvoyer le fichier index.html.twig qui se trouve dans le dossier home dans le template
    {
       /*  $entityManager = $this->getDoctrine()->getManager();
        $ecoleDoctoraleRepository= $entityManager->getRepository(EcoleDoctorale::class);
        $ecoleDoctorale= $ecoleDoctoraleRepository->findAll();

        if(empty($ecoleDoctorale)){
            $ecole1 = new  EcoleDoctorale ();
            $ecole1->setNom("UniversitéMontpellier");
            $entityManager->persist($ecole1);

            $these1 = new Theses();
            $these1->setTitre("Climat");
            $these1->setDescription("réchauffement climatique");
            $these1->setEcoleDoctorale($ecole1);
            $entityManager->persist($these1);

            $these2 = new Theses();
            $these2->setTitre("Climat2");
            $these2->setDescription("réchauffementClimatique2");
            $these2->setEcoleDoctorale($ecole1);
            $entityManager->persist($these2);

            $entityManager->flush();

        }
        */



        return $this->render('home/index.html.twig', [

            //'ecoleDoctorales'=>$ecoleDoctoraleRepository->findAll(), 
            
            'controller_name' => 'HomeController',
            'infothese' =>  [

                
                ['titre'=>'titre de la these 1','contact'=>'tassadit.benahmed@yahoo.fr','Description' =>'Description de la these 1'],
           
                ['titre'=>'titre de la these 2','contact'=>' tassadit.benahmed@yahoo.fr', 'Description' => 'Description de la these 1'],
            
                [ 'titre'=> 'titre de la these3','contact'=> 'tassadit.benahmed@yahoo.fr','Description' => 'Description de la these 1']


            ]
        ]);
    }
}
