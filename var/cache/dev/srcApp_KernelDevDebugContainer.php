<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerIOc5ftP\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerIOc5ftP/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerIOc5ftP.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerIOc5ftP\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \ContainerIOc5ftP\srcApp_KernelDevDebugContainer([
    'container.build_hash' => 'IOc5ftP',
    'container.build_id' => 'e20946c3',
    'container.build_time' => 1575823536,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerIOc5ftP');
